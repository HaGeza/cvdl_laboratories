import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras, config
from keras import layers, optimizers, losses

epochs = 10
used_metrics = ['accuracy']

class TransferResNet:
    def __init__(self, num_classes, input_shape, train_gen, val_gen):
        self.num_classes = num_classes
        self.input_shape = input_shape
        self.train_gen   = train_gen
        self.val_gen     = val_gen
        
    def build_model(self, silent=False):
        base_model = keras.applications.ResNet50(
            weights = 'imagenet',
            input_shape = self.input_shape,
            include_top = False
        )
        base_model.trainable = False

        # Take input
        inputs = keras.Input(shape=self.input_shape)
        # Preprocess input
        x = keras.applications.resnet50.preprocess_input(inputs)
        x = layers.RandomFlip("horizontal")(x)
        x = layers.RandomRotation(0.1)(x)
        x = layers.RandomZoom(0.1)(x)
        # Add base model
        x = base_model(x, training = False)
        x = layers.GlobalAveragePooling2D()(x)
        # Add classification layer
        outputs = layers.Dense(self.num_classes, 
                                kernel_initializer='he_uniform', 
                                kernel_regularizer='l2',
                                activation='softmax')(x)

        model = keras.Model(inputs, outputs)
        if not silent:
            model.summay()
        return model        
    
    def train_model(self, name, lr=3e-4, silent=True, plot=False):
        model_path = f'models/{name}'
    
        model = self.build_model(silent=silent)
        model.compile(optimizer=optimizers.adam_v2.Adam(learning_rate=lr),
                    loss=losses.CategoricalCrossentropy(), metrics=used_metrics)

        # Train model
        print(f'\tMODEL NAME: {name}')
        history = model.fit(self.train_gen, epochs=epochs, 
                            validation_data=self.val_gen)

        model.save('{}_{:.2f}'.format(model_path, history.history['val_accuracy'][-1]),
                   save_format = 'h5')
        np.save(f'history/{name}.npy',history.history)

        if plot:
            for metric in used_metrics:
                plt.plot(history.history[metric], label=metric)
                plt.title(f'{name}_train')
                plt.plot(history.history[f'val_{metric}'], label=f'val_{metric}')
                plt.title(f'{name}_val')
                plt.xlabel('Epoch')
                plt.ylabel(metric)
                plt.ylim([0, 1])
                plt.legend(loc='lower right')
            plt.show()
            
def train_models(num_classes, input_shape, train_gen, val_gen):
    physical_devices = config.list_physical_devices('GPU') 
    config.experimental.set_memory_growth(physical_devices[0], True)
    
    mini_res = TransferResNet(num_classes, input_shape, train_gen, val_gen)
    
    base_dir = 'resNet50'

    mini_res.train_model(f'{base_dir}/2e-3', 2e-3, silent=False)
    mini_res.train_model(f'{base_dir}/1e-3', 1e-3)
    mini_res.train_model(f'{base_dir}/8e-4', 8e-4)
    mini_res.train_model(f'{base_dir}/7e-4', 7e-4)
    mini_res.train_model(f'{base_dir}/5e-4', 5e-4)
    mini_res.train_model(f'{base_dir}/3e-4', 3e-4)