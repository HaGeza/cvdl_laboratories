import sys

import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras, config
from keras import backend as K, layers, optimizers, losses, models

conv_size = 3
pool_size = 2
strides   = 1
conv_filters = 20
res_filters  = 40

num_conv_layers = 5
num_res_blocks  = 5

epochs = 10
used_metrics = ['accuracy']

class MiniResNet:
    def __init__(self, num_classes, input_shape, train_gen, val_gen):
        self.num_classes = num_classes
        self.input_shape = input_shape
        self.train_gen   = train_gen
        self.val_gen     = val_gen
    
    def __resnet_block(self, inputs, num_filters, filter_size):
        x1 = layers.Conv2D(num_filters, filter_size, 1, 'same', activation='relu')(inputs)
        x2 = layers.Conv2D(num_filters, filter_size, 1, 'same', activation='relu')(x1)
        return layers.Add()([inputs, x2])

    def build_model(self, silent=False):
        img_inputs = keras.Input(shape=self.input_shape)
        
        x = layers.Conv2D(conv_filters, conv_size, strides, 'same', activation='relu')(img_inputs)
        x = layers.AveragePooling2D(pool_size)(x)
        for _ in range(num_conv_layers - 2):
            x = layers.Conv2D(conv_filters, conv_size, strides, 'same', activation='relu')(x)
            x = layers.AveragePooling2D(pool_size)(x)
        
        x = layers.Conv2D(res_filters, conv_size, strides, 'same', activation='relu')(x)
        x = layers.AveragePooling2D(pool_size)(x)
        for _ in range(num_res_blocks):
            x = self.__resnet_block(x, res_filters, conv_size)
        
        x = layers.GlobalAveragePooling2D()(x)

        class_outputs = layers.Dense(
            self.num_classes, kernel_initializer='he_uniform', activation='softmax')(x)
        
        model = keras.Model(img_inputs, class_outputs)
        if not silent:
            model.summary()
        
        return model

    def train_model(self, name, lr=3e-4, silent=True, plot=False):
        model_path = f'models/{name}'
    
        model = self.build_model(silent=silent)
        model.compile(optimizer=optimizers.adam_v2.Adam(learning_rate=lr),
                    loss=losses.CategoricalCrossentropy(), metrics=used_metrics)

        # Train model
        print(f'\tMODEL NAME: {name}')
        history = model.fit(self.train_gen, epochs=epochs, 
                            validation_data=self.val_gen)

        model.save('{}_{:.2f}'.format(model_path, history.history['val_accuracy'][-1]))
        np.save(f'history/{name}.npy',history.history)

        if plot:
            for metric in used_metrics:
                plt.plot(history.history[metric], label=metric)
                plt.title(f'{name}_train')
                plt.plot(history.history[f'val_{metric}'], label=f'val_{metric}')
                plt.title(f'{name}_val')
                plt.xlabel('Epoch')
                plt.ylabel(metric)
                plt.ylim([0, 1])
                plt.legend(loc='lower right')
            plt.show()
    
def train_models(num_classes, input_shape, train_gen, val_gen):
    physical_devices = config.list_physical_devices('GPU') 
    config.experimental.set_memory_growth(physical_devices[0], True)

    base_name = f'res_{num_conv_layers}_{num_res_blocks}'  
    
    mini_res = MiniResNet(num_classes, input_shape, train_gen, val_gen)

    mini_res.train_model(f'{base_name}/2e-3', 2e-3, silent=False)
    mini_res.train_model(f'{base_name}/1e-3', 1e-3)
    mini_res.train_model(f'{base_name}/8e-4', 8e-4)
    mini_res.train_model(f'{base_name}/7e-4', 7e-4)
    mini_res.train_model(f'{base_name}/5e-4', 5e-4)
    mini_res.train_model(f'{base_name}/3e-4', 3e-4)

    mini_res.train_model(f'{base_name}/s_exp_5e-3_1000_98',
                keras.optimizers.schedules.ExponentialDecay(5e-3, 1000, 0.98))
    mini_res.train_model(f'{base_name}/s_exp_1e-3_915_98',
                keras.optimizers.schedules.ExponentialDecay(1e-3, 915, 0.98))
    mini_res.train_model(f'{base_name}/s_exp_1e-4_1600_99',
                keras.optimizers.schedules.ExponentialDecay(1e-4, 1600, 0.99))
    mini_res.train_model(f'{base_name}/s_exp_1e-4_800_98',
                keras.optimizers.schedules.ExponentialDecay(1e-4, 800, 0.98))
    mini_res.train_model(f'{base_name}/s_exp_1e-4_1200_99',
                keras.optimizers.schedules.ExponentialDecay(1e-4, 1200, 0.99))

    # model = models.load_model('models/res_5_5/3e-4_0.149')
    # for x,y in val_generator:
    #     p = np.argmax(model.predict(x), axis=-1)

    #     fig, axes = plt.subplots(nrows=1, ncols=6, figsize=[16, 9])
    #     for i in range(len(axes)):
    #         axes[i].set_title(f'{np.argmax(y[i])} : {p[i]}')
    #         axes[i].imshow(x[i])
    #     plt.show()

