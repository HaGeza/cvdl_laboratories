import sys
import glob
import matplotlib.pyplot as plt
import numpy as np

from dataGenerator import DataGenerator
import mymodel
import transfer

import tensorflow.keras.applications.resnet50 as resnet50
import tensorflow.keras.models as models

def get_paths(argv=[]):
    arg_cnt = len(argv)
    
    if arg_cnt == 0:
        default_train_ids = 'datasets/Oxford_pets/annotations/trainval.txt'
        default_val_ids   = 'datasets/Oxford_pets/annotations/test.txt'
        default_images    = 'datasets/Oxford_pets/images'
        
        return [default_train_ids, default_val_ids, default_images, default_images]
    elif arg_cnt == 3:
        return [argv[0], argv[1], argv[2], argv[2]]
    elif arg_cnt == 4:
        return argv
    else:
        print(f'Argument count should either be:\n\
              0: Use the Oxford_pets dataset\n\
              3: Use train_ids val_ids inputs; uses the same images for both training and validation\n\
              4: Use train_ids val_ids train_inputs val_inputs')
        return None
        
paths = get_paths()
if paths == None:
    sys.exit(1)
    
batch_size = 32
num_classes = 37
input_shape = (128, 128, 3)
    
train_generator = DataGenerator(
    paths[0], paths[2], batch_size, input_shape, num_classes, shuffle=True
)
val_generator = DataGenerator(
	paths[1], paths[3], batch_size, input_shape, num_classes, shuffle=False
)

label_names = [
    "Abyssinian", "american_bulldog", "american_pit_bull_terrier",
    "basset_hound", "beagle", "Bengal", "Birman", "Bombay", "boxer",
    "British_Shorthair", "chihuahua", "Egyptian_Mau", "english_cocker_spaniel",
    "english_setter", "german_shorthaired", "great_pyrenees", "havanese",
    "japanese_chin", "keeshond", "leonberger", "Maine_Coon",
    "miniature_pinscher", "newfoundland", "Persian", "pomeranian", "pug",
    "Ragdoll", "Russian_Blue", "saint_bernard", "samoyed", "scottish_terrier",
    "shiba_inu", "Siamese", "Sphynx", "staffordshire_bull_terrier",
    "wheaten_terrier", "yorkshire_terrier"
]

# Data visualization
# Note: delete to_categorical from DataGenerator to use this snipet
# batch_x, batch_y = train_generator[0]

# fig, axes = plt.subplots(nrows=1, ncols=6, figsize=[16, 9])
# for i in range(len(axes)):
#     axes[i].set_title(label_names[batch_y[i]])
#     axes[i].imshow(batch_x[i])
# plt.show()

def view_saved_model_history(paths, metrics_to_plot='trainval'):
    for path in paths:
        print(path)
        hist = np.load(path, allow_pickle=True).tolist()
        
        _, ax = plt.subplots()
        for k,v in hist.items():
            is_val = k.startswith('val')
            if metrics_to_plot == 'train' and is_val:
                continue
            if metrics_to_plot == 'val' and not is_val:
                continue
            
            color = (np.random.rand(), np.random.rand(), np.random.rand())
            ax.plot(v, color=color, label=k)
            
        handles, labels = (ax.get_legend_handles_labels())
        ax.legend(handles, labels, bbox_to_anchor=(1.04,1), loc="upper left")
        plt.subplots_adjust(right=0.7)
       
        
        plt.show()
        
def evaluate_models(paths):
    for path in paths:
        print(path)
        model = models.load_model(path)
        model.evaluate(val_generator)

# Mini model training
# mymodel.train_models(num_classes, input_shape, train_generator, val_generator)

# Transfer learning
# transfer.train_models(num_classes, input_shape, train_generator, val_generator)

# Plot training history
view_saved_model_history(glob.glob('L4\\history\\*\\*'))

# evaluate_models(glob.glob('L4\\models\\resNet50\\*'))