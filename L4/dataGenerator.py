import numpy as np

from tensorflow import keras
from keras.preprocessing.image import load_img, img_to_array
from keras.utils.np_utils import to_categorical

class DataGenerator(keras.utils.Sequence):
    def __init__(self, ids_file, in_dir, batch_size, input_shape, num_classes, 
                 shuffle=True):
        # TODO your initialization
        # you might want to store the parameters into class variables
        self.input_shape = input_shape
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.shuffle = shuffle
        # load the data from the root directory
        self.data, self.labels = self.get_data(ids_file, in_dir)
        self.indices = np.arange(len(self.data))
        self.on_epoch_end()

    def get_data(self, ids_file, in_dir):
        """"
        Loads the paths to the images and their corresponding labels from the database directory
        """
        # TODO your code here
        ids = np.char.array([
            line.split()[:2] for line in open(ids_file, 'r').read().splitlines()
        ])
        self.data = in_dir + '/' + ids[:,0] + '.jpg'
        self.labels = ids[:,1]
        return self.data, self.labels

    def __len__(self):
        """
        Returns the number of batches per epoch: the total size of the dataset divided by the batch size
        """
        return int(np.floor(len(self.data) / self.batch_size))

    def __getitem__(self, index):
        """"
        Generates a batch of data
        """
        batch_indices = self.indices[index*self.batch_size : (index+1)*self.batch_size]
        batch_x = np.array(
            [img_to_array(load_img(file, target_size=self.input_shape))
                for file in self.data[batch_indices]], dtype=np.uint8)
        batch_y = np.array(self.labels[batch_indices]).astype(np.uint8) - 1
        batch_y = to_categorical(batch_y, num_classes=self.num_classes)
  
        return batch_x, batch_y

    def on_epoch_end(self):
        """"
        Called at the end of each epoch
        """
        # if required, shuffle your data after each epoch
        self.indices = np.arange(len(self.data))
        if self.shuffle:
            # TODO shuffle data
            # you might find np.random.shuffle useful here
            np.random.shuffle(self.indices)