import numpy as np

def softmax(x, t=1):
    """"
    Applies the softmax temperature on the input x, using the temperature t
    """
    # TODO your code here
    exps = np.exp((x - np.max(x)) / t).astype(np.float32)
    sumExps = np.sum(exps)
    return exps / sumExps
    # end TODO your code here

def softmax_array(x, t=1):
    exps = (x - np.reshape(np.max(x, axis=1), (x.shape[0], 1))) / t
    eSum = np.reshape(np.sum(np.exp(exps), axis=1), (x.shape[0], 1))
    return exps / eSum, exps, eSum