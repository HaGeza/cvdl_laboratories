import numpy as np
from lab2.activations import softmax, softmax_array

class SoftmaxClassifier:
    def __init__(self, input_shape, num_classes):
        self.input_shape = input_shape
        self.num_classes = num_classes
        self.W = None
        self.initialize()

    def initialize(self):
        # TODO your code here
        # initialize the weight matrix (remember the bias trick) with small random variables
        # you might find np.random.randn userful here *0.001
        
        # self.W = np.random.randn(self.input_shape + 1, self.num_classes) * 0.001
        self.W = np.random.randn(self.input_shape, self.num_classes).astype(np.float32) * 0.001

        pass

    def predict_proba(self, X: np.ndarray) -> np.ndarray:
        # TODO your code here
        # 0. compute the dot product between the weight matrix and the input X
        # remember about the bias trick!

        # scores = np.dot(np.append(X, [1]), self.W)
        scores = np.dot(X, self.W)

        # 1. apply the softmax function on the scores
        # 2, returned the normalized scores
        return softmax_array(scores)

    def predict(self, X: np.ndarray) -> int:
        # TODO your code here
        # 0. compute the dot product between the weight matrix and the input X as the scores
        
        # scores = np.dot(np.append(X, [1]), self.W)
        scores, _, _ = self.predict_proba(X)

        # 1. compute the prediction by taking the argmax of the class scores
        return np.argmax(scores, axis=1)

    def fit(self, X_train: np.ndarray, y_train: np.ndarray,
            **kwargs) -> dict:

        history = []

        bs = kwargs['bs'] if 'bs' in kwargs else 128
        reg_strength = kwargs['reg_strength'] if 'reg_strength' in kwargs else 1e3
        steps = kwargs['steps'] if 'steps' in kwargs else 100
        lr = kwargs['lr'] if 'lr' in kwargs else 1e-3
        print(bs, reg_strength, steps, lr)

        one_hot = np.eye(self.num_classes)
        calc_score = lambda X: softmax(np.dot(X, self.W))

        # run mini-batch gradient descent
        for iteration in range(0, steps):
            # print(f'Iteration: {iteration}')
            # TODO your code here
            # sample a batch of images from the training set
            # you might find np.random.choice useful
            indices = np.random.choice(len(X_train), bs)            # [3,6,0,7]
            X_batch, y_batch = X_train[indices], y_train[indices]   # [x3,x6,...] [y3,y6,...]

            # compute the loss and dW
            scores, exps, eSum = self.predict_proba(X_batch)
            loss = np.average(-exps[(np.arange(bs), y_batch)] + np.log(eSum)) \
                 + reg_strength * np.sum(self.W * self.W)

            dW = np.average(
                np.reshape(scores - one_hot[y_batch], (bs, 1, self.num_classes)) * \
                np.reshape(X_batch, (bs, self.input_shape, 1)), axis=0
            ) + reg_strength * self.W
            
            # end TODO your code here
            # perform a parameter update
            self.W -= lr * dW
            # append the training loss, accuracy on the training set and accuracy on the test set to the history dict
            history.append(loss)

        return history


    def get_weights(self, img_shape):
        # TODO your code here
        # 0. ignore the bias term
        W = self.W[:self.input_shape - 1, :]
        # 1. reshape the weights to (*image_shape, num_classes)
        W = np.reshape(W, (*img_shape, self.num_classes))
        return W

    def load(self, path: str) -> bool:
        # TODO your code here
        # load the input shape, the number of classes and the weight matrix from a file
        try:
            with open(path, 'rb') as f:
                self.input_shape = np.load(f)
                self.num_classes = np.load(f)
                self.W = np.load(f)
        except:
            return False
        return True

    def save(self, path: str) -> bool:
        # TODO your code here
        # save the input shape, the number of classes and the weight matrix to a file
        # you might find np.save useful for this
        try:
            with open(path, 'wb') as f:
                np.save(f, self.input_shape)
                np.save(f, self.num_classes)
                np.save(f, self.W)
        except:
            return False
        return True