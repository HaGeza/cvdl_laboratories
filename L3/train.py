import tensorflow as tf
import numpy as np

from tensorflow.keras import datasets, layers, models, initializers, regularizers
import matplotlib.pyplot as plt

(train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()

# Normalize pixel values to be between 0 and 1
train_images, test_images = train_images / 255.0, test_images / 255.0

class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',
               'dog', 'frog', 'horse', 'ship', 'truck']

class Cutout(layers.Layer):
    def __init__(self, mask_size, **kwargs):
        super().__init__(**kwargs)
        self.mask_size = mask_size
        self.tl_half = int(mask_size / 2)
        self.br_half = mask_size - self.tl_half

    def call(self, X, training=None):
        if training == False:
          return X

        H, W = X.shape[1], X.shape[2]
        center_y = np.random.randint(self.half_mask, H - self.half_mask)
        center_x = np.random.randint(self.half_mask, W - self.half_mask)

        mask = np.ones(X.shape[1:])
        mask[center_y - self.tl_half : center_y + self.br_half,
             center_x - self.tl_half : center_x + self.br_half] = 0

        return X * mask

# Create model
def create_model():
    heNormal = initializers.HeNormal()
    l2Regu = regularizers.l2(0.0005)

    model = models.Sequential()
    model.add(Cutout(2, input_shape=(32, 32, 3)))
    model.add(layers.Conv2D(32, (3, 3), activation='relu',
              kernel_initializer=heNormal, kernel_regularizer=l2Regu))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu',
              kernel_initializer=heNormal, kernel_regularizer=l2Regu))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu',
              kernel_initializer=heNormal, kernel_regularizer=l2Regu))
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu',
              kernel_initializer=heNormal, kernel_regularizer=l2Regu))
    model.add(layers.Dropout(0.05))
    model.add(layers.Dense(10, kernel_regularizer=regularizers.l2(0.0002)))

    model.summary()

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    # Train model
    history = model.fit(train_images, train_labels, epochs=10, 
                        validation_data=(test_images, test_labels))

    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.5, 1])
    plt.legend(loc='lower right')

    model.save('cutmodel')
    return model

# model = create_model()
# model = models.load_model('dropmodel')

# Test model
model = models.load_model('L3/cutmodel')
test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)
print(test_acc)

# base_model: 0.7092000246047974
# he_model:   0.7127000093460083
# regu_model: 0.7208999991416931
# drop_model: 0.7344999909400940
# cut_model:  0.7049000263214110