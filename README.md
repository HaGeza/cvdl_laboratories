# CVDL_Laboratories

Pelok Balázs-István, CVDL 2021-2022, Laboratories

- Lab1: 
  - https://colab.research.google.com/drive/1QycrXFAoFUox98mWDY_r5M9IBZgf4CES?usp=sharing
- Lab2: 
  - in L2 folder
  - https://colab.research.google.com/drive/1HY-Y4oYndJHQpBAA27426j-fXLwWmU5U?usp=sharing
- Lab3:
  - training in L3 folder
  - rest: https://colab.research.google.com/drive/1FcKAySsMTIgyWTkrNp9oRXmkIy-qcQHh?usp=sharing
- Lab4:
  - in L4 folder
  - https://colab.research.google.com/drive/1TbyPn6DYcmjol1mTFAV-ZzLn-OqMKqfp?usp=sharing
- Lab5:
  - https://colab.research.google.com/drive/1YMp014MDF19LfdznPXeLATZlhyEsXVGO?usp=sharing
- Lab6:
  - https://colab.research.google.com/drive/1R1nRMNuW-YAZWOQScpLhJ9zh3OstOugh?usp=sharing